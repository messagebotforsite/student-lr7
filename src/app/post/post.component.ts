import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Post, PostsService } from '../posts.service';
import { Location } from '@angular/common';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent  implements OnInit {
  post: Post;

  constructor(
    private location: Location,
    private router: ActivatedRoute,
    public postsService: PostsService
  ) {}

  ngOnInit(): void {
    this.router.params.pipe(
      switchMap(param => this.postsService.getById(+param.id))
    ).subscribe((post) => this.post = post);
  }

  navBack() {
    this.location.back();
  }
}