import { HttpClient } from '@angular/common/http';
import {Injectable} from '@angular/core'
import { map } from 'rxjs/operators';

export interface Post {
  title: string
  text: string
  id: number
}

interface DbPost {
  title: string
  body: string
  id: number
}

@Injectable({providedIn: 'root'})
export class PostsService {
  readonly apiUrl = 'https://jsonplaceholder.typicode.com';
  posts: Post[] = [];

  constructor(private http: HttpClient) {}

  private postAdapter({title, id, body}: DbPost): Post {
    return {id, title, text: body}
  }

  loadPosts() {
    this.http.get<DbPost[]>(`${this.apiUrl}/posts?_limit=4`).pipe(
        map(posts => posts.map(this.postAdapter))
      ).subscribe(response => this.posts = response);
  }

  getById(id: number) {
    return this.http.get<DbPost>(`${this.apiUrl}/posts/${id}`)
      .pipe(map(this.postAdapter));
  }
}
